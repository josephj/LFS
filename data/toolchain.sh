#!/bin/bash

#verify host requirements
sudo ln -sf bash /bin/sh
sudo apt-get -y install g++
sudo apt-get -y install bison
sudo apt-get -y install gawk
sudo apt-get -y install m4
sudo apt-get -y install make
sudo apt-get -y install patch
sudo apt-get -y install texinfo

#compile toolchain
export LFS=~/lfs
mkdir lfs

cd lfs
mkdir sources
chmod -v a+wt $LFS/sources
wget --input-file=/vagrant_data/wget-list --continue --directory-prefix=$LFS/sources

#TODO: add in md5 checksum

sudo mkdir -v $LFS/tools
sudo ln -sv $LFS/tools /

#TODO: add in lfs user to perform actions (root is good for now)

#setup bash_profile and .bashrc
cat > ~/.bash_profile << "EOF"
exec env -i HOME=$HOME TERM=$TERM PS1='\u:\w\$ ' /bin/bash
EOF

cat > ~/.bashrc << "EOF"
set +h
umask 022
LFS=/mnt/lfs
LC_ALL=POSIX
LFS_TGT=$(uname -m)-lfs-linux-gnu
PATH=/tools/bin:/bin:/usr/bin
export LFS LC_ALL LFS_TGT PATH
EOF

source ~/.bash_profile

#constructing a temporary system
cd sources

# Installation of cross Binutils
tar xvfj binutils-2.26.tar.bz2 
cd binutils-2.26
mkdir -v build
cd build

../configure --prefix=/tools            \
             --with-sysroot=$LFS        \
             --with-lib-path=/tools/lib \
             --target=$LFS_TGT          \
             --disable-nls              \
             --disable-werror
make
make install

#installation of Cross GCC
tar xvfj gcc-5.3.0.tar.bz2 
cd gcc-5.3.0.tar.bz2
tar -xf ../mpfr-3.1.3.tar.xz 
mv -v mpfr-3.1.3 mpfr
tar -xf ../gmp-6.1.0.tar.xz 
mv -v gmp-6.1.0 gmp
tar -xf ../mpc-1.0.3.tar.gz
mv -v mpc-1.0.3 mpc

for file in \
 $(find gcc/config -name linux64.h -o -name linux.h -o -name sysv4.h)
do
  cp -uv $file{,.orig}
  sed -e 's@/lib\(64\)\?\(32\)\?/ld@/tools&@g' \
      -e 's@/usr@/tools@g' $file.orig > $file
  echo '
#undef STANDARD_STARTFILE_PREFIX_1
#undef STANDARD_STARTFILE_PREFIX_2
#define STANDARD_STARTFILE_PREFIX_1 "/tools/lib/"
#define STANDARD_STARTFILE_PREFIX_2 ""' >> $file
  touch $file.orig
done


mkdir -v build
cd build

../configure                                       \
    --target=$LFS_TGT                              \
    --prefix=/tools                                \
    --with-glibc-version=2.11                      \
    --with-sysroot=$LFS                            \
    --with-newlib                                  \
    --without-headers                              \
    --with-local-prefix=/tools                     \
    --with-native-system-header-dir=/tools/include \
    --disable-nls                                  \
    --disable-shared                               \
    --disable-multilib                             \
    --disable-decimal-float                        \
    --disable-threads                              \
    --disable-libatomic                            \
    --disable-libgomp                              \
    --disable-libquadmath                          \
    --disable-libssp                               \
    --disable-libvtv                               \
    --disable-libstdcxx                            \
    --enable-languages=c,c++

make
make install

#linux-4.4.2 API headers
cd ../..
tar xvJf linux-4.4.2.tar.xz 
cd linux-4.4.2

make mrproper

make INSTALL_HDR_PATH=dest headers_install
cp -rv dest/include/* /tools/include
cd ..

# Installation of Glibc
tar xvf glibc-2.23.tar.xz 
cd glibc-2.23
mkdir -v build
cd build

../configure                             \
      --prefix=/tools                    \
      --host=$LFS_TGT                    \
      --build=$(../scripts/config.guess) \
      --disable-profile                  \
      --enable-kernel=2.6.32             \
      --enable-obsolete-rpc              \
      --with-headers=/tools/include      \
      libc_cv_forced_unwind=yes          \
      libc_cv_ctors_header=yes           \
      libc_cv_c_cleanup=yes

make
make install
cd ../..

#Libstdc++
cd gcc-5.3.0
sudo rm -r build
mkdir build
cd build

../libstdc++-v3/configure           \
    --host=$LFS_TGT                 \
    --prefix=/tools                 \
    --disable-multilib              \
    --disable-nls                   \
    --disable-libstdcxx-threads     \
    --disable-libstdcxx-pch         \
    --with-gxx-include-dir=/tools/$LFS_TGT/include/c++/5.3.0

make
make install

cd ../..

#Binutils Pass 2
cd binutils-2.26
rm -fr build
mkdir build
cd build

CC=$LFS_TGT-gcc                \
AR=$LFS_TGT-ar                 \
RANLIB=$LFS_TGT-ranlib         \
../configure                   \
    --prefix=/tools            \
    --disable-nls              \
    --disable-werror           \
    --with-lib-path=/tools/lib \
    --with-sysroot








